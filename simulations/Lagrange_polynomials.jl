using SymPy


function generate_poly(s, nodes,deg)
#Generates Lagrangian polynomial in symbolic

aux=[]
phi=[]

for k=1:deg+1
    append!(aux, 1);
    for j=1:deg+1
        if j != k
            aux[k]=aux[k].*(s-nodes[j]);
        end
    end
end
for j=1:deg+1
    aux[j]= aux[j]/SymPy.N(subs(aux[j],s,nodes[j]));
end
return aux
end

function compute_theta(nodes)
# Compute the integral of lagrange polynomials from 0 to interpolation point theta[r,m]= \int_{t^0}^{t^m} \phi_r
s=Sym("s");
deg=length(nodes)-1;
poly=generate_poly(s,nodes,deg)

theta=zeros(deg+1, deg+1)

for r=1:deg+1
    for m=1:deg+1
        theta[r,m]=integrate(poly[r],(s,0,nodes[m]))
    end
end

return theta

end
