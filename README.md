# Issues with Positivity Preserving Patankar-Type Schemes

[![License: MIT](https://img.shields.io/badge/License-MIT-success.svg)](https://opensource.org/licenses/MIT)


This repository contains the code for the reproducibility of "Issues with Positivity-Preserving Patankar-type Schemes" by D. Torlo, P. Öffner and H. Ranocha [arXiv preprint](https://arxiv.org/abs/2108.07347).


We provide some *Julia* codes and Jupyter notebooks to reproduce the simulations of the paper, some *Mathematica* notebook to reproduce the symbolic computations and the results of the paper, and the supplementary material file with all the results that could not fit in the article.

### Julia codes
In folder [simulations](simulations/) there are:
* the implementations of all the Patankar and modified Patankar schemes ([Patankar Runge--Kutta](simulations/mPRK.jl) and [modified Patankar Deferred Correction](simulations/mPDeC.jl)) and some auxiliary functions ([Lagrange polynomials file](simulations/Lagrange_polynomials.jl))
* the study of the general **linear $`2\times 2`$ system** searching for the maximum $`\Delta t`$ bound for which we do not observe oscillations for all the schemes is in the [CFL notebook](simulations/SearchingCFLconditions.ipynb)
* the study of the spurious steady state for all the methods is in the [Inconsistency notebook](simulations/Inconsistency.ipynb)
* the study of direction of the first time step for all the methods varying $`0<\varepsilon<\theta<1`$ [Direction notebook](simulations/CheckingDirection.ipynb)
* the test of the oscillation-free bounds previously found on **scalar nonlinear problem** is in [Nonlinear Problem notebook](simulations/Nonlinear_Problems.ipynb)
* the test of the inconsistency on **Robertson problem** is in [Robertson notebook](simulations/Robertson_Problem.ipynb)
* the test of the inconsistency on **HIRES problem** is in [HIRES notebook](simulations/Problem_HIRES.ipynb)


### Mathematica notebooks
In folder [symbolic_computations](symbolic_computations/) contains:
* the computations of the $`\Delta t`$ bound on the general **linear $`2\times 2`$ system** for the **MPRK(2,2,1)** scheme is in the [MPRK(2,2,1) notebook](symbolic_computations/MPRK_2_2_1_generalSystem.nb)
* the computations of Taylor expansions to understand which scheme has inconsistency or loss of accuracy order on the **symmetric linear $`2\times 2`$ system** in the limit $`u_1^0 \to 0`$ for some of the **MPRK(2,2,$`\alpha`$)** schemes is in the [MPRK(2,2,$`\alpha`$) notebook](symbolic_computations/MPRK_2_2_alpha.nb)
* the Taylor expansion to understand if **MPRK(3,2)** has inconsistency on the **symmetric linear $`2\times 2`$ system** is in the [MPRK(3,2) notebook](symbolic_computations/MPRK_3_2.nb)
* the Taylor expansion to understand if **mPDeC2** and **mPDeC3** have inconsistencies on the **symmetric linear $`2\times 2`$ system** is in the [MPDeC notebook](symbolic_computations/MPDeC.nb)
* the computations of the stability functions of many **RK** methods and the check of their properties on **positivity** and **correct direction** are in [RK properties notebook](symbolic_computations/RK_properties.nb)

### Supplementary material
In folder [supplementary_material](supplementary_material/) there is the [file](supplementary_material/supplementary_material.pdf) with the supplementary results which are only described in the report 


## Disclaimer

Everything is provided as-is and without warranty. Use at your own risk!
